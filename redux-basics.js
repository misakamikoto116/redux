// jalankannya node redux-basics.js
// createStore Default dari redux
// Yang di dispatch ada type (Default) : Itu harus Uppercase, dan ada juga value(ini sembarang aja)
// Dispatch Akan dipakai di reducer(anggapannya dispatch itu seperti didMount dia update), reducer akan dipakai di store
// Subscription ('Memberikan pesan setelah di dispatch') : output = [subscription] {counter : angkanya}, jadi subscription ini selalu trigger setelah dispatch / something changed
const redux = require('redux')
const createStore = redux.createStore;

const initialState = {
    counter: 0
}

// Reducer
const rootReducer = (state = initialState, action) => {
    if (action.type === 'INC_COUNTER') {
        return {
            ...state,
            counter: state.counter + 1
        }
    }

    if (action.type === 'ADD_COUNTER') {
        return {
            ...state,
            counter: state.counter + action.value
        }
    }

    return state
}

// Store
const store = createStore(rootReducer);
console.log(store.getState())

// Subscription
store.subscribe(() => {
    console.log('[Subcribtion]', store.getState())
})

// Dispacthing Action
store.dispatch({type: 'INC_COUNTER'})
store.dispatch({type: 'ADD_COUNTER', value: 10})
console.log(store.getState())