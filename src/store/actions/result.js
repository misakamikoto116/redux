//return (dispatch, getState) : bawaan redux-thunk
// Kenapa statenya .ctr dulu? karena di index.js rootReducer diset begitu

import * as actionTypes from './actionTypes'

export const saveResult = (counterState) => {
    return {
        type: actionTypes.STORE_RESULT,
        counter: counterState
    }
}

export const storeResult = (counterState) => {
    return (dispatch, getState) => {
        setTimeout(() => {
            // const oldState = getState().ctr.counter;
            // console.log(oldState);
            dispatch(saveResult(counterState))
        }, 2000)
    }
}

export const deleteResult = (id) => {
    return {
        type: actionTypes.DELETE_RESULT,
        rsltId: id
    }
}