// actions nya di import buat ngambil nama dari action nya yang udh di set disana
// Tujuannya : jika terjadi typo maka akan ada error
import * as actionTypes from '../actions/actionTypes';
import {updateObject} from '../utility'
const initialState = {
    counter: 0,
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.INCREMENT:
            return updateObject(state, {counter: state.counter + 1})
        case actionTypes.DECREMENT:
            return updateObject(state, {counter: state.counter - 1})
        case actionTypes.ADD:
            return updateObject(state, {counter: state.counter + action.value})
        case actionTypes.SUBTRACT:
            return updateObject(state, {counter: state.counter - action.value})
    }

    return state
}

export default reducer