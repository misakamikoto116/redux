// Concat kaya push, tapi concat copy yang lama baru ubah yang baru
// actions nya di import buat ngambil nama dari action nya yang udh di set disana
// Tujuannya : jika terjadi typo maka akan ada error
// Jadi ngambil state.counter nya itu dari parsingan data di jsx nya (yang action)
import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility'
const initialState = {
    results: []
}

const deleteResult = (state, action) => {
    const id = action.rsltId;
    const newState = [...state.results];
    const filterState = newState.filter(rslt => rslt.id !== id);

    return updateObject(state, {results: filterState})
}

const storeResult = (state, action) => {
    return updateObject(state, {
        results: state.results.concat({
            id: Math.random(),
            value: action.counter
        })
    })
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.STORE_RESULT:
            return storeResult(state, action)
        case actionTypes.DELETE_RESULT:
           return deleteResult(state, action);
    }

    return state
}

export default reducer