// Konsepnya, kita membuat state di reducer, lalu parsing lagi kesini dengan HOC sebagai props ctr dibawah, nah ctr: state.counter itu untuk milih state mana yang dipakai di reducer, lalu dijadikan props ctr 
// connect(mapStateToProps)(Counter) : memparsing data mapStateToProps ke Counter / class ini
// Jika tidak mau slice dan hanya mau ngedispatch doang maka gunakan connect(null, mapDispatchToProps)(Counter)
// mapDispatchToProps : membuat props dari dispatch reducer, cara panggil : this.props.onIncrementCounter
// type: actionTypes.INCREMENT ngambil const INCREMENT di actions.js, jadi kalau ada typo pasti ada error
import React, { Component } from 'react';
import { connect } from 'react-redux'

import CounterControl from '../../components/CounterControl/CounterControl';
import CounterOutput from '../../components/CounterOutput/CounterOutput';
import * as actionCreator from '../../store/actions/index'

class Counter extends Component {
    counterChangedHandler = ( action, value ) => {
        switch ( action ) {
            case 'inc':
                this.setState( ( prevState ) => { return { counter: prevState.counter + 1 } } )
                break;
            case 'dec':
                this.setState( ( prevState ) => { return { counter: prevState.counter - 1 } } )
                break;
            case 'add':
                this.setState( ( prevState ) => { return { counter: prevState.counter + value } } )
                break;
            case 'sub':
                this.setState( ( prevState ) => { return { counter: prevState.counter - value } } )
                break;
        }
    }

    render () {
        return (
            <div>
                <CounterOutput value={this.props.ctr} />
                <CounterControl label="Increment" clicked={this.props.onIncrementCounter} />
                <CounterControl label="Decrement" clicked={this.props.onDecrementCounter}  />
                <CounterControl label="Add 10" clicked={this.props.onAddCounter}  />
                <CounterControl label="Subtract 15" clicked={this.props.onSubtractCounter}  />
                <hr />
                <button onClick={() => this.props.onStoreResult(this.props.ctr)}>Store Results</button>
                <ul>
                    {this.props.storeResults.map(strResult => (
                        <li key={strResult.id}
                            onClick={() => this.props.onDeleteResult(strResult.id)}>
                            {strResult.value}
                        </li>
                    ))}
                </ul>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        ctr: state.ctr.counter,
        storeResults: state.res.results
    };
}

const mapDispatchToProps = dispatch => {
    return {
        onIncrementCounter:         () => dispatch( actionCreator.increment() ),
        onDecrementCounter:         () => dispatch( actionCreator.decrement() ),
        onAddCounter:               () => dispatch( actionCreator.add(10) ),
        onSubtractCounter:          () => dispatch( actionCreator.subtract(15) ),
        onStoreResult: (counterState) => dispatch( actionCreator.storeResult(counterState) ),
        onDeleteResult: (id) => dispatch( actionCreator.deleteResult(id) )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Counter);